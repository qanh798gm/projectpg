const initialState = {
    email: '',
    name: '',
    _id: '',
    age: 0,
    token: '',
    cart: [],
    component: [],
    accessory: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                email: action.payload.email,
                token: action.payload.token,
                _id: action.payload._id
            }
        case 'SET_USER_AFTER_REGISTER':
            return {
                ...state,
                email: action.payload.email,
                name: action.payload.name,
                age: action.payload.age,
                token: action.payload.token,
                _id: action.payload._id
            }
        case 'SET_COMPONENT':
            return {
                ...state,
                component: action.payload.component
            }
        case 'SET_ACCESSORY':
            return {
                ...state,
                accessory: action.payload.accessory
            }
    }
    return state
}

export default reducer

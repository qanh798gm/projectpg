import React, { Component } from 'react';
import { Router, Route, Link } from "react-router-dom";
import axios from 'axios'
import { connect } from 'react-redux'

class Navigation extends Component {
    state = {
        components: [],
        accessories: [],
    }

    componentDidMount() {
        axios.get('http://localhost:3001/categories/Component').then(response => {
            //this.props.setComponentValue(response.data)

            this.setState({ components: response.data })
        })
        axios.get('http://localhost:3001/categories/Accessory').then(response => {
            //this.props.setAccessoryValue(response.data)
            console.log(response.data)
            this.setState({ accessories: response.data })
        })
    }

    render() {
        const components = this.state.components.map(component => {
            return (
                <li><Link to={`/product-list/${component._id}`}>{component.name}</Link></li>
            )
        })
        const accessories = this.state.accessories.map(accessory => {
            return (
                <li><Link to="/product-list">{accessory.name}</Link></li>
            )
        })
        return (
            <div className="container">
                <div className="navigation">
                    <nav className="navbar navbar-default">
                        {/* Brand and toggle get grouped for better mobile display */}
                        <div className="navbar-header nav_2">
                            <button type="button" className="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                            </button>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul className="nav navbar-nav">
                                <li><Link to="/" className="act">Home</Link></li>
                                {/* Mega Menu */}
                                <li className="dropdown">
                                    <a className="dropdown-toggle" data-toggle="dropdown">Products <b className="caret" /></a>
                                    <ul className="dropdown-menu multi-column columns-3">
                                        <div className="row">
                                            <div className="col-sm-3">
                                                <ul className="multi-column-dropdown">
                                                    <h6>Components</h6>
                                                    {components}
                                                </ul>
                                            </div>
                                            <div className="col-sm-3">
                                                <ul className="multi-column-dropdown">
                                                    <h6>Accessories</h6>
                                                    {accessories}
                                                    {/* <li><a href="products1.html">Mouse</a></li>
                                                    <li><a href="products1.html">Keyboard</a></li>
                                                    <li><a href="products1.html">Headphone<span>New</span></a></li> */}
                                                </ul>
                                            </div>
                                            <div className="col-sm-2">
                                                <ul className="multi-column-dropdown">
                                                    <h6>Home</h6>
                                                    <li><a href="products2.html">Tv</a></li>
                                                    <li><a href="products2.html">Camera</a></li>
                                                    <li><a href="products2.html">AC</a></li>
                                                    <li><a href="products2.html">Grinders</a></li>
                                                </ul>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="w3ls_products_pos">
                                                    <h4>30%<i>Off/-</i></h4>
                                                    <img src="images/1.jpg" alt=" " className="img-responsive" />
                                                </div>
                                            </div>
                                            <div className="clearfix" />
                                        </div>
                                    </ul>
                                </li>
                                <li><a href="about.html">About Us</a></li>
                                <li className="w3pages"><a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span className="caret" /></a>
                                    <ul className="dropdown-menu">
                                        <li><a href="icons.html">Web Icons</a></li>
                                        <li><a href="codes.html">Short Codes</a></li>
                                    </ul>
                                </li>
                                <li><a href="mail.html">Mail Us</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        )
    }
}

// const mapStateToProps = state => {
//     return {
//         components: state.component,
//         accessories: state.accessory
//     }
// }

// const mapDispatchToProps = dispatch => {
//     return {
//         setComponentValue: (data) => dispatch({
//             type: 'SET_COMPONENT',
//             payload: {
//                 component: data
//             }
//         }),
//         setAccessoryValue: (data) => dispatch({
//             type: 'SET_ACCESSORY',
//             payload: {
//                 accessory: data
//             }
//         })
//     }
// }

export default Navigation;
import React, { Component } from 'react'

export default class CartContent extends Component {
    render() {
        return (
            <div className="container">
                <div className="row" style={{ paddingTop: '25px', paddingBottom: '25px' }}>
                    <div className="col-md-12">
                        <div id="mainContentWrapper">
                            <div className="col-md-8 col-md-offset-2">
                                <h2 style={{ textAlign: 'center' }}>
                                    Review Your Order &amp; Complete Checkout
                            </h2>
                                <hr />
                                <div className="shopping_cart">
                                    <form className="form-horizontal" role="form" action method="post" id="payment-form">
                                        <div className="panel-group" id="accordion">
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h4 className="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Review
                                                Your Order</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" className="panel-collapse collapse in">
                                                    <div className="panel-body">
                                                        <table id="cart" className="table table-hover table-condensed">
                                                            <thead>
                                                                <tr>
                                                                    <th style={{ width: '50%' }}>Product</th>
                                                                    <th style={{ width: '10%' }}>Price</th>
                                                                    <th style={{ width: '8%' }}>Quantity</th>
                                                                    <th style={{ width: '22%' }} className="text-center">Subtotal</th>
                                                                    <th style={{ width: '10%' }} />
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td data-th="Product">
                                                                        <div className="row">
                                                                            <div className="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." className="img-responsive" /></div>
                                                                            <div className="col-sm-10">
                                                                                <h4 className="nomargin">Product 1</h4>
                                                                                <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td data-th="Price">$150.00</td>
                                                                    <td data-th="Quantity">
                                                                        <input type="number" className="form-control text-center" defaultValue={1} />
                                                                    </td>
                                                                    <td data-th="Subtotal" className="text-center">150.00</td>
                                                                    <td className="actions" data-th>
                                                                        <button className="btn btn-info btn-sm"><i className="fa fa-refresh" /></button>
                                                                        <button className="btn btn-danger btn-sm"><i className="fa fa-trash-o" /></button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td data-th="Product">
                                                                        <div className="row">
                                                                            <div className="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." className="img-responsive" /></div>
                                                                            <div className="col-sm-10">
                                                                                <h4 className="nomargin">Product 1</h4>
                                                                                <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td data-th="Price">$99.00</td>
                                                                    <td data-th="Quantity">
                                                                        <input type="number" className="form-control text-center" defaultValue={1} />
                                                                    </td>
                                                                    <td data-th="Subtotal" className="text-center">99.00</td>
                                                                    <td className="actions" data-th>
                                                                        <button className="btn btn-info btn-sm"><i className="fa fa-refresh" /></button>
                                                                        <button className="btn btn-danger btn-sm"><i className="fa fa-trash-o" /></button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr className="visible-xs">
                                                                    <td className="text-center"><strong>Total 1.99</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><a className="btn btn-warning"><i className="fa fa-angle-left" /> Continue Shopping</a></td>
                                                                    <td colSpan={2} className="hidden-xs" />
                                                                    <td className="hidden-xs text-center"><strong>Total $150.00</strong></td>
                                                                    <td><a className="btn btn-success btn-block">Checkout <i className="fa fa-angle-right" /></a></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React from 'react';

const RelatedProducts = () => (
    <div className="w3l_related_products">
        <div className="container">
            <h3>Related Products</h3>
            <ul id="flexiselDemo2">
                <li>
                    <div className="w3l_related_products_grid">
                        <div className="agile_ecommerce_tab_left mobiles_grid">
                            <div className="hs-wrapper hs-wrapper3">
                                <img src="images/34.jpg" alt=" " className="img-responsive" />
                                <img src="images/35.jpg" alt=" " className="img-responsive" />
                                <img src="images/27.jpg" alt=" " className="img-responsive" />
                                <img src="images/28.jpg" alt=" " className="img-responsive" />
                                <img src="images/37.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <div className="flex_ecommerce">
                                        <a href="#" data-toggle="modal" data-target="#myModal6"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </div>
                                </div>
                            </div>
                            <h5><a href="single.html">Kid's Toy</a></h5>
                            <div className="simpleCart_shelfItem">
                                <p className="flexisel_ecommerce_cart"><span>$150</span> <i className="item_price">$100</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Kid's Toy" />
                                    <input type="hidden" name="amount" defaultValue={100.00} />
                                    <button type="submit" className="w3ls-cart">Add to cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="w3l_related_products_grid">
                        <div className="agile_ecommerce_tab_left mobiles_grid">
                            <div className="hs-wrapper hs-wrapper3">
                                <img src="images/36.jpg" alt=" " className="img-responsive" />
                                <img src="images/32.jpg" alt=" " className="img-responsive" />
                                <img src="images/33.jpg" alt=" " className="img-responsive" />
                                <img src="images/32.jpg" alt=" " className="img-responsive" />
                                <img src="images/36.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <div className="flex_ecommerce">
                                        <a href="#" data-toggle="modal" data-target="#myModal5"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </div>
                                </div>
                            </div>
                            <h5><a href="single.html">Vacuum Cleaner</a></h5>
                            <div className="simpleCart_shelfItem">
                                <p className="flexisel_ecommerce_cart"><span>$960</span> <i className="item_price">$920</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Vacuum Cleaner" />
                                    <input type="hidden" name="amount" defaultValue={920.00} />
                                    <button type="submit" className="w3ls-cart">Add to cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="w3l_related_products_grid">
                        <div className="agile_ecommerce_tab_left mobiles_grid">
                            <div className="hs-wrapper hs-wrapper3">
                                <img src="images/38.jpg" alt=" " className="img-responsive" />
                                <img src="images/37.jpg" alt=" " className="img-responsive" />
                                <img src="images/27.jpg" alt=" " className="img-responsive" />
                                <img src="images/28.jpg" alt=" " className="img-responsive" />
                                <img src="images/37.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <div className="flex_ecommerce">
                                        <a href="#" data-toggle="modal" data-target="#myModal3"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </div>
                                </div>
                            </div>
                            <h5><a href="single.html">Microwave Oven</a></h5>
                            <div className="simpleCart_shelfItem">
                                <p className="flexisel_ecommerce_cart"><span>$650</span> <i className="item_price">$645</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Microwave Oven" />
                                    <input type="hidden" name="amount" defaultValue={645.00} />
                                    <button type="submit" className="w3ls-cart">Add to cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div className="w3l_related_products_grid">
                        <div className="agile_ecommerce_tab_left mobiles_grid">
                            <div className="hs-wrapper hs-wrapper3">
                                <img src="images/p3.jpg" alt=" " className="img-responsive" />
                                <img src="images/p5.jpg" alt=" " className="img-responsive" />
                                <img src="images/p4.jpg" alt=" " className="img-responsive" />
                                <img src="images/p2.jpg" alt=" " className="img-responsive" />
                                <img src="images/p1.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <div className="flex_ecommerce">
                                        <a href="#" data-toggle="modal" data-target="#myModal4"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </div>
                                </div>
                            </div>
                            <h5><a href="single.html">Music MP3 Player</a></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$60</span> <i className="item_price">$58</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Ultra MP3 Player" />
                                    <input type="hidden" name="amount" defaultValue={58.00} />
                                    <button type="submit" className="w3ls-cart">Add to cart</button>
                                </form>
                            </div>
                            <div className="mobiles_grid_pos">
                                <h6>New</h6>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
);

export default RelatedProducts;
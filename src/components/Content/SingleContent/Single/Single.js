import React, { Component } from 'react';
import axios from 'axios'

class Single extends Component {
    state = {
        product: {}
    }

    componentDidMount() {
        
        axios.get(`http://localhost:3001/product-detail/${this.props.match.params.id}`).then(response => {
            console.log(response.data)
            this.setState({ product: response.data })
        })
    }
    render() {
        const product = this.state.product
        return (
            <div className="single">
                <div className="container">
                    <div className="col-md-4 single-left">
                        <div className="flexslider">
                            <ul className="slides">
                                <li data-thumb="images/a.jpg">
                                    <div className="thumb-image"> <img src="images/a.jpg" data-imagezoom="true" className="img-responsive" alt /> </div>
                                </li>
                                <li data-thumb="images/b.jpg">
                                    <div className="thumb-image"> <img src="images/b.jpg" data-imagezoom="true" className="img-responsive" alt /> </div>
                                </li>
                                <li data-thumb="images/c.jpg">
                                    <div className="thumb-image"> <img src="images/c.jpg" data-imagezoom="true" className="img-responsive" alt /> </div>
                                </li>
                            </ul>
                        </div>
                        {/* flexslider */}
                        <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
                        {/* flexslider */}
                        {/* zooming-effect */}
                        {/* //zooming-effect */}
                    </div>
                    <div className="col-md-8 single-right">
                        <h3>{product.name}</h3>
                        <div className="rating1">
                            <span className="starRating">
                                <input id="rating5" type="radio" name="rating" defaultValue={5} />
                                <label htmlFor="rating5">5</label>
                                <input id="rating4" type="radio" name="rating" defaultValue={4} />
                                <label htmlFor="rating4">4</label>
                                <input id="rating3" type="radio" name="rating" defaultValue={3} defaultChecked />
                                <label htmlFor="rating3">3</label>
                                <input id="rating2" type="radio" name="rating" defaultValue={2} />
                                <label htmlFor="rating2">2</label>
                                <input id="rating1" type="radio" name="rating" defaultValue={1} />
                                <label htmlFor="rating1">1</label>
                            </span>
                        </div>
                        <div className="description">
                            <h5><i>Description</i></h5>
                            <p>{product.description}</p>
                        </div>
                        <div className="color-quality">
                            <div className="color-quality-left">
                                <h5>Color : </h5>
                                <ul>
                                    <li><a href="#"><span /></a></li>
                                    <li><a href="#" className="brown"><span /></a></li>
                                    <li><a href="#" className="purple"><span /></a></li>
                                    <li><a href="#" className="gray"><span /></a></li>
                                </ul>
                            </div>
                            <div className="color-quality-right">
                                <h5>Quality :</h5>
                                <div className="quantity">
                                    <div className="quantity-select">
                                        <div className="entry value-minus1">&nbsp;</div>
                                        <div className="entry value1"><span>1</span></div>
                                        <div className="entry value-plus1 active">&nbsp;</div>
                                    </div>
                                </div>
                                {/*quantity*/}
                                {/*quantity*/}
                            </div>
                            <div className="clearfix"> </div>
                        </div>
                        <div className="occasional">
                            <h5>RAM :</h5>
                            <div className="colr ert">
                                <div className="check">
                                    <label className="checkbox"><input type="checkbox" name="checkbox" defaultChecked /><i> </i>3 GB</label>
                                </div>
                            </div>
                            <div className="colr">
                                <div className="check">
                                    <label className="checkbox"><input type="checkbox" name="checkbox" /><i> </i>2 GB</label>
                                </div>
                            </div>
                            <div className="colr">
                                <div className="check">
                                    <label className="checkbox"><input type="checkbox" name="checkbox" /><i> </i>1 GB</label>
                                </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>
                        <div className="simpleCart_shelfItem">
                            <p><span>$460</span> <i className="item_price">{product.price}</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Smart Phone" />
                                <input type="hidden" name="amount" defaultValue={450.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                    <div className="clearfix"> </div>
                </div>
            </div>
        )
    }
}
export default Single;
import React from 'react';

const BreadCrumbDress = () => (
    <div className="breadcrumb_dress">
        <div className="container">
            <ul>
                <li><a href="index.html"><span className="glyphicon glyphicon-home" aria-hidden="true" /> Home</a> <i>/</i></li>
                <li>Single Page</li>
            </ul>
        </div>
    </div>
);

export default BreadCrumbDress;
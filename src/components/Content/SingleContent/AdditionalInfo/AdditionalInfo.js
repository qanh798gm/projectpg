import React from 'react';

const AdditionalInfo = () => (
    <div className="additional_info">
        <div className="container">
            <div className="sap_tabs">
                <div id="horizontalTab1" style={{ display: 'block', width: '100%', margin: '0px' }}>
                    <ul>
                        <li className="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Product Information</span></li>
                        <li className="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Reviews</span></li>
                    </ul>
                    <div className="tab-1 resp-tab-content additional_info_grid" aria-labelledby="tab_item-0">
                        <h3>The Best 3GB RAM Mobile Phone</h3>
                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                          eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
                          Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                          odit aut fugit, sed quia consequuntur magni dolores eos qui
                          ratione voluptatem sequi nesciunt.Ut enim ad minima veniam, quis nostrum
                          exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea
                          commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate
                          velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat
              quo voluptas nulla pariatur.</p>
                    </div>
                    <div className="tab-2 resp-tab-content additional_info_grid" aria-labelledby="tab_item-1">
                        <h4>(2) Reviews</h4>
                        <div className="additional_info_sub_grids">
                            <div className="col-xs-2 additional_info_sub_grid_left">
                                <img src="images/t1.png" alt=" " className="img-responsive" />
                            </div>
                            <div className="col-xs-10 additional_info_sub_grid_right">
                                <div className="additional_info_sub_grid_rightl">
                                    <a href="single.html">Laura</a>
                                    <h5>Oct 06, 2016.</h5>
                                    <p>Quis autem vel eum iure reprehenderit qui in ea voluptate
                                      velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat
                    quo voluptas nulla pariatur.</p>
                                </div>
                                <div className="additional_info_sub_grid_rightr">
                                    <div className="rating">
                                        <div className="rating-left">
                                            <img src="images/star-.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star-.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star-.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>
                                </div>
                                <div className="clearfix"> </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>
                        <div className="additional_info_sub_grids">
                            <div className="col-xs-2 additional_info_sub_grid_left">
                                <img src="images/t2.png" alt=" " className="img-responsive" />
                            </div>
                            <div className="col-xs-10 additional_info_sub_grid_right">
                                <div className="additional_info_sub_grid_rightl">
                                    <a href="single.html">Michael</a>
                                    <h5>Oct 04, 2016.</h5>
                                    <p>Quis autem vel eum iure reprehenderit qui in ea voluptate
                                      velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat
                    quo voluptas nulla pariatur.</p>
                                </div>
                                <div className="additional_info_sub_grid_rightr">
                                    <div className="rating">
                                        <div className="rating-left">
                                            <img src="images/star-.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star-.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="rating-left">
                                            <img src="images/star.png" alt=" " className="img-responsive" />
                                        </div>
                                        <div className="clearfix"> </div>
                                    </div>
                                </div>
                                <div className="clearfix"> </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>
                        <div className="review_grids">
                            <h5>Add A Review</h5>
                            <form action="#" method="post">
                                <input type="text" name="Name" defaultValue="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required />
                                <input type="email" name="Email" placeholder="Email" required />
                                <input type="text" name="Telephone" defaultValue="Telephone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Telephone';}" required />
                                <textarea name="Review" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Add Your Review';}" required defaultValue={"Add Your Review"} />
                                <input type="submit" defaultValue="Submit" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default AdditionalInfo;
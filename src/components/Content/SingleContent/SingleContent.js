import React from 'react';
import Auxilinary from '../../Auxilinary';
import BreadCrumbDress from './BreadCrumbDress/BreadCrumbDress';
import Single from './Single/Single';
import AdditionalInfo from './AdditionalInfo/AdditionalInfo';
import RelatedProducts from './RelatedProducts/RelatedProducts';

const SingleContent = () => (
    <Auxilinary>
        <BreadCrumbDress />
        <Single />
        <AdditionalInfo />
        <RelatedProducts />
    </Auxilinary>
)

export default SingleContent;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios'
import './images/46.jpg'
import './images/47.jpg'

class ListContent extends Component {
    state = {
        products: []
    };

    componentDidMount() {
        axios.get(`http://localhost:3001/products/${this.props.match.params.categoryID}`).then(response => {

            this.setState({ products: response.data })
            console.log(response.data)
        })
    }
    render() {
        const products = this.state.products.map(product => {
            return (
                <div className="col-md-4 agileinfo_new_products_grid agileinfo_new_products_grid_mobiles">
                    <div className="agile_ecommerce_tab_left mobiles_grid">
                        <div className="hs-wrapper hs-wrapper2">
                            <img src="images/31.jpg" alt=" " className="img-responsive" />

                            <div className="w3_hs_bottom w3_hs_bottom_sub1">
                                <ul>
                                    <li>
                                        <Link to={`/product-detail/${product._id}`} data-toggle="modal" data-target="#myModal9"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5><Link to={`/product-detail/${product._id}`} >{product.name}</Link></h5>
                        <div className="simpleCart_shelfItem">
                            <p><span>$250</span> <i className="item_price">${product.price}</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Smart Phone" />
                                <input type="hidden" name="amount" defaultValue={245.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                        <div className="mobiles_grid_pos">
                            <h6>New</h6>
                        </div>
                    </div>
                </div>
            )
        })
        const brands = this.state.products.map(product => {
            return (
                <li><Link to="/">{product.brand}</Link></li>
            )
        })
        return (
            <div className="mobiles">
                <div className="container">
                    <div className="w3ls_mobiles_grids">
                        <div className="col-md-4 w3ls_mobiles_grid_left">
                            <div className="w3ls_mobiles_grid_left_grid">
                                <h3>Categories</h3>
                                <div className="w3ls_mobiles_grid_left_grid_sub">
                                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingOne">
                                                <h4 className="panel-title asd">
                                                    <a className="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        <span className="glyphicon glyphicon-plus" aria-hidden="true" /><i className="glyphicon glyphicon-minus" aria-hidden="true" />
                                                        Brands
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div className="panel-body panel_text">
                                                    <ul>
                                                        {brands}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingTwo">
                                                <h4 className="panel-title asd">
                                                    <a className="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        <span className="glyphicon glyphicon-plus" aria-hidden="true" /><i className="glyphicon glyphicon-minus" aria-hidden="true" />Accessories
                </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div className="panel-body panel_text">
                                                    <ul>
                                                        <li><a href="products2.html">Grinder</a></li>
                                                        <li><a href="products2.html">Heater</a></li>
                                                        <li><a href="products2.html">Kid's Toys</a></li>
                                                        <li><a href="products2.html">Filters</a></li>
                                                        <li><a href="products2.html">AC</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="panel_bottom">
                                        <li><a href="products.html">Summer Store</a></li>
                                        <li><a href="products.html">Featured Brands</a></li>
                                        <li><a href="products.html">Today's Deals</a></li>
                                        <li><a href="products.html">Latest Watches</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="w3ls_mobiles_grid_left_grid">
                                <h3>Color</h3>
                                <div className="w3ls_mobiles_grid_left_grid_sub">
                                    <div className="ecommerce_color">
                                        <ul>
                                            <li><a href="#"><i /> Red(5)</a></li>
                                            <li><a href="#"><i /> Brown(2)</a></li>
                                            <li><a href="#"><i /> Yellow(3)</a></li>
                                            <li><a href="#"><i /> Violet(6)</a></li>
                                            <li><a href="#"><i /> Orange(2)</a></li>
                                            <li><a href="#"><i /> Blue(1)</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="w3ls_mobiles_grid_left_grid">
                                <h3>Price</h3>
                                <div className="w3ls_mobiles_grid_left_grid_sub">
                                    <div className="ecommerce_color ecommerce_size">
                                        <ul>
                                            <li><a href="#">Below $ 100</a></li>
                                            <li><a href="#">$ 100-500</a></li>
                                            <li><a href="#">$ 1k-10k</a></li>
                                            <li><a href="#">$ 10k-20k</a></li>
                                            <li><a href="#">$ Above 20k</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8 w3ls_mobiles_grid_right">
                            <div className="col-md-6 w3ls_mobiles_grid_right_left">
                                <div className="w3ls_mobiles_grid_right_grid1">
                                    <img src="images/46.jpg" alt=" " className="img-responsive" />
                                    <div className="w3ls_mobiles_grid_right_grid1_pos1">
                                        <h3>Smart Phones<span>Up To</span> 15% Discount</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 w3ls_mobiles_grid_right_left">
                                <div className="w3ls_mobiles_grid_right_grid1">
                                    <img src="./images/47.jpg" alt=" " className="img-responsive" />
                                    <div className="w3ls_mobiles_grid_right_grid1_pos">
                                        <h3>Top 10 Latest<span>Mobile </span>&amp; Accessories</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix"> </div>
                            <div className="w3ls_mobiles_grid_right_grid2">
                                <div className="w3ls_mobiles_grid_right_grid2_left">
                                    <h3>Showing Results: 0-1</h3>
                                </div>
                                <div className="w3ls_mobiles_grid_right_grid2_right">
                                    <select name="select_item" className="select_item">
                                        <option selected="selected">Default sorting</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by average rating</option>
                                        <option>Sort by newness</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                                <div className="clearfix"> </div>
                            </div>
                            <div className="w3ls_mobiles_grid_right_grid3">
                                {products}
                                <div className="clearfix"> </div>
                            </div>
                        </div>
                        <div className="clearfix"> </div>
                    </div>

                </div>
            </div>
        )
    }
};

export default ListContent;
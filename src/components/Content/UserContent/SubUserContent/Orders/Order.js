import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap'
import { connect } from 'react-redux'

class Order extends Component {
    state = {
        orders: []
    }

    componentDidMount() {
        axios.get('http://localhost:3001/orders', { headers: { "Authorization": `Bearer ${this.props.token}` } }).then(response => {
            this.setState({ orders: response.data })
            console.log(this.state.orders)
            //console.log(response.data)
        })
    }

    render() {
        const orders = this.state.orders.map(order => {
            return (
                <tr>
                    <td>1</td>
                    <td>{order.product[0].quantity}</td>
                    <td>{order.totalPrice}</td>
                    <td>{order.createdAt}</td>
                </tr>
            )
        })
        return (
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Total products</th>
                        <th>Total Price</th>
                        <th>Time created</th>
                    </tr>
                </thead>
                <tbody>
                    {orders}
                </tbody>
            </Table>
        )
    }
}
const mapStateToProps = state => {
    return {
        token: state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUserValue: (data) => dispatch({
            type: 'SET_USER',
            payload: {
                _id: data.user._id,
                email: data.user.email,
                token: data.token
            }
        })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Order)
import React, { Component } from 'react'

export default class OrderDetail extends Component {
    render() {
        return (
            <form className="form-horizontal" role="form" action method="post" id="payment-form">
                <div className="panel-group" id="accordion">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Order Detail
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" className="panel-collapse collapse in">
                            <div className="panel-body">
                                <table id="cart" className="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <th style={{ width: '50%' }}>Product</th>
                                            <th style={{ width: '10%' }}>Price</th>
                                            <th style={{ width: '8%' }}>Quantity</th>
                                            <th style={{ width: '22%' }} className="text-center">Subtotal</th>
                                            <th style={{ width: '10%' }} />
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-th="Product">
                                                <div className="row">
                                                    <div className="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." className="img-responsive" /></div>
                                                    <div className="col-sm-10">
                                                        <h4 className="nomargin">Product 1</h4>
                                                        <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-th="Price">$150.00</td>
                                            <td data-th="Quantity">
                                                <input type="number" className="form-control text-center" defaultValue={1} />
                                            </td>
                                            <td data-th="Subtotal" className="text-center">150.00</td>
                                        </tr>
                                        <tr>
                                            <td data-th="Product">
                                                <div className="row">
                                                    <div className="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." className="img-responsive" /></div>
                                                    <div className="col-sm-10">
                                                        <h4 className="nomargin">Product 1</h4>
                                                        <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td data-th="Price">$99.00</td>
                                            <td data-th="Quantity">
                                                <input type="number" className="form-control text-center" defaultValue={1} />
                                            </td>
                                            <td data-th="Subtotal" className="text-center">99.00</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr className="visible-xs">
                                            <td className="text-center"><strong>Total 1.99</strong></td>
                                        </tr>
                                        <tr>
                                            <td colSpan={2} className="hidden-xs" />
                                            <td />
                                            <td className="hidden-xs text-center"><strong>Total $150.00</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

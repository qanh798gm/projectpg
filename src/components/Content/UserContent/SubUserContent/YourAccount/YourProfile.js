import React from 'react';

const YourProfile = () => {
    return (
        <div className="col-md-5">
            <div className="form-area">
                <form role="form">
                    <br style={{ clear: 'both' }} />
                    <h3 style={{ marginBottom: '25px', textAlign: 'center' }}>Contact Form</h3>
                    <div className="form-group">
                        <input type="text" className="form-control" id="name" name="name" placeholder="Name" required />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="email" name="email" placeholder="Email" required />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="mobile" name="mobile" placeholder="Password" required />
                    </div>
                    <button type="button" id="submit" name="submit" className="btn btn-primary pull-right">Submit Form</button>
                </form>
            </div>
        </div>
    );
};

export default YourProfile;

import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import YourProfile from './SubUserContent/YourAccount/YourProfile';
import Order from './SubUserContent/Orders/Order';
import OrderDetail from './SubUserContent/Orders/OrderDetail';
import { Tabs, Tab, Nav, NavItem, Container, Row, Col } from 'react-bootstrap'


const UserContent = () => {
    return (
        <div>
            <div className="row">
                <br />
                <div className="col-md-8 col-md-offset-5">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default"><Link to="/user/account/profile">Profile</Link></button>
                        <button type="button" class="btn btn-default"><Link to="/user/orders">Orders</Link></button>
                    </div>
                </div>
                <br />
                <div className="col-md-4 col-md-offset-4">
                    <Route path="/user/account/profile" component={YourProfile} />
                    <Route path="/user/orders" component={Order} />
                    <Route path="/user/order-detail" component={OrderDetail} />
                </div>
            </div>
        </div>

    )
};

export default UserContent;
{/* <div className="container">
            <nav className="navbar navbar-default navbar-static-top">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand">User's Profile</a>
                    </div>
                </div>
            </nav>
            <div className="container-fluid">
                <div className="col col-md-3">
                    <div className="panel-group" id="accordion">
                        {/* Profile */}
// <div className="panel panel-default">
//     <div className="panel-heading">
//         <h4 className="panel-title">
//             <a data-toggle="collapse" data-parent="accordion" href="collapse1">
//                 Your Account</a>
//         </h4>
//     </div>
//     <div id="collapse1" className="panel-collapse collapse in">
//         <ul className="list-group">
//             <li className="list-group-item">
//                 <span className="badge">3</span>
//                 <Link to="/user/account/profile">Your Profile</Link>
//             </li>
//             <li className="list-group-item"><span className="badge">2</span>Payment</li>
//             <li className="list-group-item"><span className="badge">1</span>Address</li>
//             <li className="list-group-item"><span className="badge">1</span>Change Password</li>
//         </ul>
//     </div>
// </div>
// {/* Orders */}
// <div className="panel panel-default">
//     <div className="panel-heading">
//         <h4 className="panel-title">
//             <a data-toggle="collapse" data-parent="accordion" href="collapse2">
//                 Orders</a>
//         </h4>
//     </div>
//     <div id="collapse2" className="panel-collapse collapse">
//         <ul className="list-group">
//             <li className="list-group-item">
//                 <span className="badge">1</span>
//                 <Link to="/user/orders">Awaiting Payment</Link>
//             </li>
//             <li className="list-group-item"><span className="badge">1</span> Awaiting Pickup</li>
//             <li className="list-group-item"><span className="badge">1</span> Awaiting Shipment</li>
//             <li className="list-group-item"><span className="badge">1</span> Shipped</li>
//             <li className="list-group-item"><span className="badge">1</span> Cancelled</li>
//         </ul>
//     </div>
// </div> */}
{/* Notification */ }
{/* <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                        Notification
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" className="panel-collapse collapse">
                                <ul className="list-group">
                                    <li className="list-group-item"><span className="badge">1</span>From Your Orders</li>
                                    <li className="list-group-item"><span className="badge">5</span>Deals</li>
                                </ul>
                            </div>
                        </div> */}
{/* Voucher & Acoin */ }
{/* <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4 className="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                        Voucher & Acoin
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" className="panel-collapse collapse">
                                <ul className="list-group">
                                    <li className="list-group-item"><span className="badge">1</span> Users Reported</li>
                                    <li className="list-group-item"><span className="badge">5</span> User Waiting Activation</li>
                                </ul>
                            </div>
                        </div> */}
{/* </div>
                </div>
                <div className="col col-md-9">
                    <Route path="/user/account/profile" component={YourProfile} />
                    <Route path="/user/orders" component={Order} />
                    <Route path="/user/order-detail" component={OrderDetail} />
                </div>
            </div>
        </div> */}
import React from 'react';
import { Carousel } from 'antd'

const Banner = () => (
    <div class="container">
        <br />
        <Carousel autoplay>
            <div>
                <img className="center-block" src="images/banner/1.jpg" height="500px" />
            </div>
            <div>
                <img className="center-block" src="images/banner/2.jpg" height="500px" />
            </div>
            <div>
                <img className="center-block" src="images/banner/3.jpg" height="500px" />
            </div>
        </Carousel>
        <br />
    </div >


);

export default Banner;
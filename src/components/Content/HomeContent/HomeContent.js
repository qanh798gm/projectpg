import React from 'react';
import Auxilinary from '../../Auxilinary';
import Banner from './Banner/Banner';
import BannerBottom from './BannerBottom/BannerBottom';
import BannerBottom1 from './BannerBottom1/BannerBottom1';
import SpecialDeals from './SpecialDeals/SpecialDeals';
import NewProducts from './NewProducts/NewProducts';
import TopBrands from './TopBrands/TopBrands';


const HomeContent = () => (
    <Auxilinary>
        <Banner />
        <BannerBottom />
        <BannerBottom1 />
        <SpecialDeals />
        <NewProducts />
        <TopBrands />
    </Auxilinary>
)

export default HomeContent;
import React from 'react';
import { Carousel } from 'antd';
import 'antd/dist/antd.css';
const TopBrands = () => (
    <div className="top-brands">
        <div className="container">
            <h3>Top Brands</h3>
            <Carousel autoplay>
                <div><h2>1</h2></div>
                <div><h2>2</h2></div>
            </Carousel>
        </div>
    </div>

);

export default TopBrands;
import React from 'react';

const NewProducts = () => (
    <div className="new-products">
        <div className="container">
            <h3>New Products</h3>
            <div className="agileinfo_new_products_grids">
                <div className="col-md-3 agileinfo_new_products_grid">
                    <div className="agile_ecommerce_tab_left agileinfo_new_products_grid1">
                        <div className="hs-wrapper hs-wrapper1">
                            <img src="images/25.jpg" alt=" " className="img-responsive" />
                            <img src="images/23.jpg" alt=" " className="img-responsive" />
                            <img src="images/24.jpg" alt=" " className="img-responsive" />
                            <img src="images/22.jpg" alt=" " className="img-responsive" />
                            <img src="images/26.jpg" alt=" " className="img-responsive" />
                            <div className="w3_hs_bottom w3_hs_bottom_sub">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#myModal2"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5><a href="single.html">Laptops</a></h5>
                        <div className="simpleCart_shelfItem">
                            <p><span>$520</span> <i className="item_price">$500</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Red Laptop" />
                                <input type="hidden" name="amount" defaultValue={500.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 agileinfo_new_products_grid">
                    <div className="agile_ecommerce_tab_left agileinfo_new_products_grid1">
                        <div className="hs-wrapper hs-wrapper1">
                            <img src="images/27.jpg" alt=" " className="img-responsive" />
                            <img src="images/28.jpg" alt=" " className="img-responsive" />
                            <img src="images/29.jpg" alt=" " className="img-responsive" />
                            <img src="images/30.jpg" alt=" " className="img-responsive" />
                            <img src="images/31.jpg" alt=" " className="img-responsive" />
                            <div className="w3_hs_bottom w3_hs_bottom_sub">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5><a href="single.html">Black Phone</a></h5>
                        <div className="simpleCart_shelfItem">
                            <p><span>$380</span> <i className="item_price">$370</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Black Phone" />
                                <input type="hidden" name="amount" defaultValue={370.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 agileinfo_new_products_grid">
                    <div className="agile_ecommerce_tab_left agileinfo_new_products_grid1">
                        <div className="hs-wrapper hs-wrapper1">
                            <img src="images/34.jpg" alt=" " className="img-responsive" />
                            <img src="images/33.jpg" alt=" " className="img-responsive" />
                            <img src="images/32.jpg" alt=" " className="img-responsive" />
                            <img src="images/35.jpg" alt=" " className="img-responsive" />
                            <img src="images/36.jpg" alt=" " className="img-responsive" />
                            <div className="w3_hs_bottom w3_hs_bottom_sub">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#myModal5"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5><a href="single.html">Kids Toy</a></h5>
                        <div className="simpleCart_shelfItem">
                            <p><span>$150</span> <i className="item_price">$100</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Kids Toy" />
                                <input type="hidden" name="amount" defaultValue={100.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 agileinfo_new_products_grid">
                    <div className="agile_ecommerce_tab_left agileinfo_new_products_grid1">
                        <div className="hs-wrapper hs-wrapper1">
                            <img src="images/37.jpg" alt=" " className="img-responsive" />
                            <img src="images/38.jpg" alt=" " className="img-responsive" />
                            <img src="images/39.jpg" alt=" " className="img-responsive" />
                            <img src="images/40.jpg" alt=" " className="img-responsive" />
                            <img src="images/41.jpg" alt=" " className="img-responsive" />
                            <div className="w3_hs_bottom w3_hs_bottom_sub">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#myModal6"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5><a href="single.html">Induction Stove</a></h5>
                        <div className="simpleCart_shelfItem">
                            <p><span>$280</span> <i className="item_price">$250</i></p>
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" defaultValue="_cart" />
                                <input type="hidden" name="add" defaultValue={1} />
                                <input type="hidden" name="w3ls_item" defaultValue="Induction Stove" />
                                <input type="hidden" name="amount" defaultValue={250.00} />
                                <button type="submit" className="w3ls-cart">Add to cart</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="clearfix"> </div>
            </div>
        </div>
    </div>

);

export default NewProducts;
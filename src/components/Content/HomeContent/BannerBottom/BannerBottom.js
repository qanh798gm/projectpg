import React from 'react';
import { Router, Route, Link } from "react-router-dom";
import { Carousel } from 'antd';


const BannerBottom = () => (
    <div className="banner-bottom">
        <div className="container">
            <h2>Just Arrival</h2>
            <div className="agile_ecommerce_tabs">
                <div className="col-md-4 agile_ecommerce_tab_left">
                    <Carousel autoplay>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                    </Carousel>
                </div>
                <div className="col-md-4 agile_ecommerce_tab_left">
                    <Carousel autoplay>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                    </Carousel>
                </div>
                <div className="col-md-4 agile_ecommerce_tab_left">
                    <Carousel autoplay>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                        <div>
                            <div className="hs-wrapper">
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <img src="images/6.jpg" alt=" " className="img-responsive" />
                                <img src="images/7.jpg" alt=" " className="img-responsive" />
                                <img src="images/3.jpg" alt=" " className="img-responsive" />
                                <img src="images/4.jpg" alt=" " className="img-responsive" />
                                <img src="images/5.jpg" alt=" " className="img-responsive" />
                                <div className="w3_hs_bottom">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal"><span className="glyphicon glyphicon-eye-open" aria-hidden="true" /></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><Link to="/product-detail">Mobile Phone1</Link></h5>
                            <div className="simpleCart_shelfItem">
                                <p><span>$380</span> <i className="item_price">$350</i></p>
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" defaultValue="_cart" />
                                    <input type="hidden" name="add" defaultValue={1} />
                                    <input type="hidden" name="w3ls_item" defaultValue="Mobile Phone1" />
                                    <input type="hidden" name="amount" defaultValue={350.00} />
                                </form>
                            </div>
                        </div>
                    </Carousel>
                </div>
                <div className="clearfix"> </div>
            </div>
            <div className="clearfix"> </div>
        </div>
    </div>
);
export default BannerBottom;
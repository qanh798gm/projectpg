import React, { Component } from 'react'
import axios from 'axios'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Form, Button, Row, Col } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'


class LoginContent extends Component {
    state = {
        email: '',
        password: '',
        token: ''
    }

    postDataHandler = () => {
        const account = {
            email: this.state.email,
            password: this.state.password
        }
        axios.post('http://localhost:3001/users/login', account).then(response => {
            console.log(response.data)
            if (response.data.token.length > 0) {
                this.props.setUserValue(response.data)
                this.props.history.push('/')
                console.log(response.data.token)
            }
            else {
                console.log('error')
            }
        })
    }

    render() {
        return (
            <div className="row">
                <br />
                <div className="col-md-4 col-md-offset-4">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <center><h2 className="panel-title">Please sign in</h2></center>
                        </div>
                        <div className="panel-body">
                            <fieldset>
                                <div className="form-group">
                                    <input value={this.state.email} onChange={this.handleInputChange}
                                        onChange={(event) => this.setState({ email: event.target.value })}
                                        className="form-control" placeholder="E-mail" name="email" type="text" />
                                </div>
                                <div className="form-group">
                                    <input value={this.state.password} onChange={this.handleInputChange}
                                        onChange={(event) => this.setState({ password: event.target.value })}
                                        className="form-control" placeholder="Password" name="password" type="password" />
                                </div>
                                <div className="col-md-4 col-md-offset-4">
                                    <input onClick={this.postDataHandler} className="btn btn-lg btn-success btn-block" defaultValue="Login" />
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token,
        email: state.email,
        _id: state._id
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUserValue: (data) => dispatch({
            type: 'SET_USER',
            payload: {
                _id: data.user._id,
                email: data.user.email,
                token: data.token
            }
        })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginContent)

/* <div className="row">
                    <br />
                    <br />
                    <div className="col-md-4 col-md-offset-4">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <center><h2 className="panel-title">Please sign in</h2></center>
                            </div>
                            <div className="panel-body">
                                <fieldset>
                                    <div className="form-group">
                                        <input value={this.state.email} onChange={this.handleInputChange}
                                            onChange={(event) => this.setState({ email: event.target.value })}
                                            className="form-control" placeholder="E-mail" name="email" type="text" />
                                    </div>
                                    <div className="form-group">
                                        <input value={this.state.password} onChange={this.handleInputChange}
                                            onChange={(event) => this.setState({ password: event.target.value })}
                                            className="form-control" placeholder="Password" name="password" type="password" />
                                    </div>
                                    <input onClick={this.postDataHandler} className="btn btn-lg btn-success btn-block" defaultValue="Login" />
                                    <text>Token Text Here: {this.props.token}</text>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div> */
import React, { Component } from 'react'
import axios from 'axios'
import { connect } from 'react-redux'

class RegisterContent extends Component {
    state = {
        email: '',
        password: '',
        name: '',
        age: 0,
        type: 'user'
    }

    postDataHandler = () => {
        const account = {
            email: this.state.email,
            password: this.state.password,
            name: this.state.name,
            age: this.state.age,
            type: this.state.type
        }
        axios.post('http://localhost:3001/users', account).then(response => {
            console.log(this.state)
            console.log(response.data)
            if (response.data.token.length > 0) {
                this.props.setUserAfterRegisterValue(response.data)
                this.props.history.push('/')
                console.log(response.data.token)
            }
            else {
                console.log('error')
            }
        })
    }

    render() {
        return (
            <div className="row">
                <br />
                <div className="col-md-4 col-md-offset-4">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <center><h2 className="panel-title">Please sign up</h2></center>
                        </div>
                        <div className="panel-body">
                            <fieldset>
                                <div className="form-group">
                                    <input value={this.state.email}
                                        onChange={(event) => this.setState({ email: event.target.value })}
                                        className="form-control" placeholder="E-mail" name="email" type="text" />
                                </div>
                                <div className="form-group">
                                    <input value={this.state.password} 
                                        onChange={(event) => this.setState({ password: event.target.value })}
                                        className="form-control" placeholder="Password" name="password" type="password" />
                                </div>
                                <div className="form-group">
                                    <input value={this.state.name} 
                                        onChange={(event) => this.setState({ name: event.target.value })}
                                        className="form-control" placeholder="Name" name="name" type="text" />
                                </div>
                                <div className="form-group">
                                    <input value={this.state.age} 
                                        onChange={(event) => this.setState({ age: event.target.value })}
                                        className="form-control" placeholder="Age" name="age" type="number" />
                                </div>
                                <div className="col-md-4 col-md-offset-4">
                                    <input onClick={this.postDataHandler} className="btn btn-lg btn-success btn-block" defaultValue="Register" />
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUserAfterRegisterValue: (data) => dispatch({
            type: 'SET_USER_AFTER_REGISTER',
            payload: {
                _id: data.user._id,
                email: data.user.email,
                name: data.user.name,
                age: data.user.age,
                token: data.token
            }
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContent)
import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import HomeContent from '../components/Content/HomeContent/HomeContent';
import ListContent from './Content/ListContent/ListContent';
import CartContent from './Content/CartContent/CartContent';
import CheckoutContent from './Content/CheckoutContent/CheckoutContent';
import UserContent from './Content/UserContent/UserContent';
import Single from './Content/SingleContent/Single/Single';
import LoginContent from './Content/AccountContent/LoginContent/LoginContent';
import RegisterContent from './Content/AccountContent/RegisterContent/RegisterContent';


export default class Layout extends Component {
    render() {
        return (
            <div>
                <Route exact path="/" component={HomeContent} />
                <Route path="/product-detail/:id" component={Single} />
                <Route path="/product-list/:categoryID" component={ListContent} />
                <Route path="/cart" component={CartContent} />
                <Route path="/checkout/" component={CheckoutContent} />
                <Route path="/user" component={UserContent} />
                <Route path="/login" component={LoginContent} />
                <Route path="/register" component={RegisterContent} />
            </div>
        )
    }
}

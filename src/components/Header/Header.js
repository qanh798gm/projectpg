import React, { Component } from 'react'
import { Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'


class Header extends Component {
    render() {
        return (
            <div className="header" id="home1">
                <div className="container">
                    <div>
                        <div className="w3l_login">
                            <Link to="/user" ><span className="glyphicon glyphicon-user" aria-hidden="true" /></Link>
                        </div>
                        <div class="btn-group" role="group" aria-label="...">
                            <button type="button" class="btn btn-default"><Link to="/login">Login</Link></button>
                            <button type="button" class="btn btn-default"><Link to="/register">Register</Link></button>
                        </div>
                        <text>Hello {this.props.email}</text>
                    </div>
                    <div className="w3l_logo">
                        <h1><Link to="/">Electronic Store<span>Computer Components - Accessories</span></Link></h1>
                    </div>
                    {/* search */}
                    <ul className="nav navbar-nav navbar-right">
                        <li className="dropdown">
                            <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span className="glyphicon glyphicon-shopping-cart" /> 7 - Items<span className="caret" /></a>
                            <ul className="dropdown-menu dropdown-cart" role="menu">
                                <li>
                                    <span className="item">
                                        <span className="item-left">
                                            <img src="http://lorempixel.com/50/50/" alt />
                                            <span className="item-info">
                                                <span>Item name</span>
                                                <span>23$</span>
                                            </span>
                                        </span>
                                        <span className="item-right">
                                            <button className="btn btn-xs btn-danger pull-right">x</button>
                                        </span>
                                    </span>
                                </li>
                                <li className="divider" />
                                <li><Link className="text-center" to="/checkout">View Cart</Link></li>
                            </ul>
                        </li>
                    </ul>
                    
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        email: state.email
    }
}

export default connect(mapStateToProps)(Header);

import React, { Component } from 'react';
//import './App.css';
import Header from './components/Header/Header';
import Navigation from './components/Navigation/Navigation'
import Footer from './components/Footer/Footer';
import Layout from './components/Layout';
import Auxilinary from './components/Auxilinary';
import { BrowserRouter } from 'react-router-dom';
import SearchBar from './components/Content/SearchBar/SearchBar';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Auxilinary>
          <Header />
          <SearchBar />
          <Navigation />

          {/* The shared-partial - Layout*/}
          <Layout />
          {/* The shared-partial - Layout */}
          
          <Footer />
        </Auxilinary>
      </BrowserRouter>
    );
  }
}

export default App;

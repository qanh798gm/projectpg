const redux = require('redux')
const createStore = redux.createStore

const initialState = {
    email: '',
    password: '',
    token: ''
}

//Reducer
const rootReducer = (state = initialState, action) => {
    if (action.type === 'SET_EMAIL') {
        return {
            ...state,
            email: action.payload.email
        }
    }
    if (action.type === 'SET_PASSWORD') {
        return {
            ...state,
            password: action.payload.password
        }
    }
    return state;
}

//Store
const store = createStore(rootReducer)
console.log(store.getState())

//Dispatching Action
store.dispatch({
    type: 'SET_EMAIL', payload: {
        email: 'qa@example.com'
    }
})
store.dispatch({
    type: 'SET_PASSWORD', payload: {
        password: 'anh798GM'
    }
})

console.log(store.getState())

//Sucscription
